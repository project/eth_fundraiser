# Ethereum Fundraiser contributed module for Drupal 8.x || 9

## Contents of this file

- Introduction
- Requirements
- Installation
- Configuration
- Troubleshooting
- Maintainer

## Introduction

The Ethereum Fundraising Component facilitates the making of contributions to a community or cause by target audiences on the Ethereum blockchain.
Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.

## Requirements

This module requires Drupal core >= 8.0.

## Installation

Install like any other Drupal Module or use composer.
```
composer require drupal/eth_fundraiser

```

## Configuration

Navigate to :

Structure » Place Block » Ethereum Fundraiser

Configure the ERC20 address (Ethereum MainNet Address/Wallet) where you would like to receive Ethereum along with any other settings or styles you desire.

- Disable Block Title.
- Click Save block.

Choose footer region to place the block:

## Troubleshooting

If you don't see your Eth Fundraiser do the following
- Clear cache.
- Make sure you added the Eth Fundraiser to your Block Layout.
- Make sure you saved the settings on Eth Fundraiser Form.

## Maintainer

Current Maintainers
- Martelus https://www.drupal.org/u/martelus
