<?php

namespace Drupal\eth_fundraiser\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'EthFundraiser' block.
 *
 * @Block(
 *  id = "eth_fundraiser_block",
 *  admin_label = @Translation("Eth Fundraiser Block"),
 *  category = @Translation("Eth Fundraiser"),
 * )
 */
class EthFundraiserBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['markup1'] = [
      '#type' => 'markup',
      '#markup' => '
        <div class="btn-group">
          <a class="button js-form-submit form-submit" href="https://martelus.club" target="_blank" class="btn-martelus-club">Get Club Access</a>
          <a class="button js-form-submit form-submit"  href="https://martelus.net" target="_blank" class="btn-martelus-io">Create an Account</a> 
        </div>
        <p class="welcome-message">
          The Eth Fundraiser Component allows you to raise funds through the ethereum blockchain. It also allows you to customize the component to match your site theme.<br>
          If you find that you need additional functionality there is a club member version available which offers multiple currencies, chain selection, token selection, additional wallet detection, installation assistance, technical support and more.
        </p>
      ',
    ];

    $form['address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ethereum Address'),
      '#placeholder' => '0x',
      '#description' => $this->t('Use your ethereum address to use the component to send funds to your address.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config['address'],
    ];

    $form['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Banner Position'),
      '#description' => $this->t('Select a position for the banner to appear.'),
      '#options' => [
        'none' => $this->t('None'),
        'left' => $this->t('Left'),
        'center' => $this->t('Center'),
        'right' => $this->t('Right'),
      ],
      '#size' => 0,
      '#default_value' => $config['position'],
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Banner Text"),
      '#description' => $this->t('Change the banner title text.'),
      '#size' => 64,
      '#default_value' => !empty($config["title"]) ? $config["title"] : "Buy us a coffee",
    ];

    $form['titleColor'] = [
      '#type' => 'color',
      '#title' => $this->t("Banner Color Text"),
      '#description' => $this->t('Change the banner title color text.'),
      '#size' => 7,
      '#default_value' => $config["titleColor"] != "#000000" ? $config["titleColor"] : "#000000",
    ];

    $form['svgCircle'] = [
      '#type' => 'color',
      '#title' => $this->t('SVG Circle Background'),
      '#description' => $this->t("Change the circle background color"),
      '#size' => 7,
      '#default_value' => $config["svgCircle"] != "#000000" ? $config["svgCircle"] : "#14044d",
    ];

    $form['svgEthIcon'] = [
      '#type' => 'color',
      '#title' => $this->t('SVG Eth Icon Color'),
      '#description' => $this->t("Change the eth icon color"),
      '#size' => 7,
      '#default_value' => $config["svgEthIcon"] != "#000000" ? $config["svgEthIcon"] : "#ffffff",
    ];

    $form['bannerBackground'] = [
      '#type' => 'color',
      '#title' => $this->t("Banner Background"),
      '#description' => $this->t('Change the banner background.'),
      '#size' => 7,
      '#default_value' => $config["bannerBackground"] != "#000000" ? $config["bannerBackground"] : "#ffffff",
    ];

    $form['buttonBackground'] = [
      '#type' => 'color',
      '#title' => $this->t("Button Background"),
      '#description' => $this->t('Change the button background.'),
      '#size' => 7,
      '#default_value' => $config["buttonBackground"] != "#000000" ? $config["buttonBackground"] : "#14044d",
    ];

    $form['buttonText'] = [
      '#type' => 'color',
      '#title' => $this->t("Button Text Color"),
      '#description' => $this->t('Change the button text color.'),
      '#size' => 7,
      '#default_value' => $config["buttonText"] != "#000000" ? $config["buttonText"] : "#ffffff",
    ];

    $form['closeBtnColor'] = [
      '#type' => 'color',
      '#title' => $this->t("Close Button Color"),
      '#description' => $this->t('Change the close button color.'),
      '#size' => 7,
      '#default_value' => $config["closeBtnColor"] != "#000000" ? $config["closeBtnColor"] : "#808080",
    ];

    $form['ethPriceColor'] = [
      '#type' => 'color',
      '#title' => $this->t("Eth Price Color"),
      '#description' => $this->t('Change eth price text color.'),
      '#size' => 7,
      '#default_value' => $config["ethPriceColor"] != "#000000" ? $config["ethPriceColor"] : "#fff",
    ];

    $form['markup8'] = [
      '#type' => 'markup',
      '#markup' => '
      <div>
        <h3>Attention</h3>
        <p>Remember after saving your changes to clear cache.</p>
      </div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state;

    $this->configuration["address"] = $form_state->getValue("address");
    $this->configuration["title"] = $form_state->getValue("title");
    $this->configuration["position"] = $form_state->getValue("position");
    $this->configuration["titleColor"] = $form_state->getValue("titleColor");
    $this->configuration["svgCircle"] = $form_state->getValue("svgCircle");
    $this->configuration["svgEthIcon"] = $form_state->getValue("svgEthIcon");
    $this->configuration["bannerBackground"] = $form_state->getValue("bannerBackground");
    $this->configuration["buttonBackground"] = $form_state->getValue("buttonBackground");
    $this->configuration["buttonText"] = $form_state->getValue("buttonText");
    $this->configuration["closeBtnColor"] = $form_state->getValue("closeBtnColor");
    $this->configuration["ethPriceColor"] = $form_state->getValue("ethPriceColor");

  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $build = [];
    $build['#theme'] = 'eth_fundraiser_block';

    $items = [];
    $items['address'] = $this->configuration['address'];
    $items['position'] = $this->configuration['position'];

    $items['title'] = $this->configuration['title'];
    $items['titleColor'] = $this->configuration['titleColor'];

    $items['bannerBackground'] = $this->configuration['bannerBackground'];
    $items['buttonBackground'] = $this->configuration['buttonBackground'];
    $items['buttonText'] = $this->configuration['buttonText'];
    $items['svgCircle'] = $this->configuration['svgCircle'];
    $items['svgEthIcon'] = $this->configuration['svgEthIcon'];
    $items['closeBtnColor'] = $this->configuration['closeBtnColor'];
    $items['ethPriceColor'] = $this->configuration['ethPriceColor'];
    $build['#content'] = $items;

    return $build;
  }

}
